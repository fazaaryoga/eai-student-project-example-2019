package com.adf.tugasakhir.repository;

import com.adf.tugasakhir.dataclass.Paper;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaperRepository extends CrudRepository<Paper, Long> {
}
