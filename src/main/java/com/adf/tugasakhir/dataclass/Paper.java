package com.adf.tugasakhir.dataclass;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "paper")
public class Paper {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    private String title;

    private String abstrak;

    private String url;

    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }

    @Override
    public String toString() {
        return "Paper{" + "title='" + title + '\'' + ", abstrak='" + abstrak + '\'' + ", url='" + url + '\'' + '}';
    }
}
