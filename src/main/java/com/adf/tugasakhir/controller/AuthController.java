package com.adf.tugasakhir.controller;

import java.security.Principal;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthController {

  @GetMapping("/login")
  @PreAuthorize("permitAll()")
  public String login(final Principal principal) {
    if (principal != null) {
      return "redirect:/";
    }
    return "_login.html";
  }
}
