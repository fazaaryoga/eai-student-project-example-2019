# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> ###Faza Aulia Aryoga | 1806173525

TODO: Write the answers related to the programming exam here.

> Task 1 : Ensuring 12 factors

Done
1. There was an issue with the text analysis api key being an unsecured variable so the variable
has been moved to an environment variable named TEXT_API_KEY

> Task 2 : Measuring number of method invocation  

Done, extractKeyPhrases() method can be queried from a prometheus instance under the metric name "extract"

> Task 3 : Refactor classes in model layer  

Done, added @Data annotation to the conference class

> Task 4 : Implement data persistence for paper class

Done, annotated paper class with @Entity annotation and created a new PaperRepository class
that stores individual paper class instance. A postgres database has also been created for the
paperRepository class

> Task 5 : Improve code quality


Heroku link: [https://fazaaryoga-advprog-finals.herokuapp.com](https://fazaaryoga-advprog-finals.herokuapp.com)
